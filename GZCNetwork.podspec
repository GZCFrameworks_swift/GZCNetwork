#
# Be sure to run `pod lib lint GZCNetwork.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GZCNetwork'
  s.version          = '0.2.0'
  s.summary          = '网络请求库'

  s.description      = <<-DESC
  网络请求支持模块，基于Moya框架, 同时集成了ReachabilitySwift用于判断网络状态
                       DESC

  s.homepage         = 'https://gitlab.com/GZCFrameworks_swift/GZCNetwork'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guo ZhongCheng' => 'gzhongcheng@qq.com' }
  s.source           = { :git => 'https://gitlab.com/GZCFrameworks_swift/GZCNetwork.git', :tag => s.version.to_s }
  
  s.swift_version = "5.1"
  s.ios.deployment_target = '10.0'

  s.default_subspec = "Core"
  s.dependency 'ReachabilitySwift'

  s.subspec "Core" do |ss|
    ss.source_files  = "GZCNetwork/Classes/GZCNetworkResponse.swift","GZCNetwork/Classes/GZCReachability.swift","GZCNetwork/Classes/GZCTargetType.swift","GZCNetwork/Classes/NetworkConfig.swift","GZCNetwork/Classes/NetworkManager.swift"
    ss.dependency 'Moya'
    ss.dependency 'MoyaMapper'
    ss.dependency 'MoyaMapper/MMCache'
  end
  
  s.subspec "Rx" do |ss|
    ss.source_files = "GZCNetwork/Classes/RxNetworkManager.swift"
    ss.dependency 'GZCNetwork/Core'
    ss.dependency 'MoyaMapper/Rx'
    ss.dependency 'MoyaMapper/RxCache'
  end
  
end
