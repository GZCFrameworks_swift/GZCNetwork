//
//  UIImageViewExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import Kingfisher
import CoreFoundation

private var imageDownloadTaskKey: Void?

public extension UIImageView {
    /// 图片下载Task
    private var imageDownloadTask: DownloadTask? {
        get { return objc_getAssociatedObject(self, &imageDownloadTaskKey) as? DownloadTask }
        set { objc_setAssociatedObject(self, &imageDownloadTaskKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)}
    }
    
    /// 下载完成结果回调
    typealias GZCImageLoadResult = (Result<KFCrossPlatformImage?, KingfisherError>) -> Void
    
    /// 为减少列表加载图片的卡顿，加载网络图片时使用自己的缓存策略，限制图片的最大宽高进行缓存（宽高超过的话会先压缩再缓存压缩后的图片，并且设置独立的缓存Key）
    /// - Parameters:
    ///   - urlString: 图片地址
    ///   - cacheOriginImage: 是否缓存原图，默认为否
    ///   - cacheKey: 缓存key，不传则对压缩的图片采用原地址+_resize_+限制的宽度/高度 格式进行缓存
    ///   - maxWidth: 限制的最大宽度，可不传，内部会自动乘上屏幕的scale
    ///   - maxHeight: 限制的最大高度，可不传，内部会自动乘上屏幕的scale
    ///   - progressBlock: 进度回调
    ///   - completionHandler: 加载完成回调
    func loadWebImage(
        _ urlString: String,
        cacheOriginImage: Bool = false,
        cacheKey: String? = nil,
        maxWidth: CGFloat? = nil,
        maxHeight: CGFloat? = nil,
        progressBlock: DownloadProgressBlock? = nil,
        completionHandler: GZCImageLoadResult? = nil
    ) {
        /// 拼装Key
        var key: String!
        
        /// 为减少缓存图片的数量，将图片向上使用 divPoint 取整进行缓存
        let divPoint: Int = 50
        var width: Int? = maxWidth != nil ? Int(maxWidth!) : nil
        var height: Int? = maxHeight != nil ? Int(maxHeight!) : nil
        var reizeString = "_resize_"
        if width != nil {
            let remainder = width! % divPoint
            width = width! - (remainder > 0 ? (remainder - divPoint) : 0)
            reizeString += "width_\(width!)"
        }
        if height != nil {
            let remainder = height! % divPoint
            height = height! - (remainder > 0 ? (remainder - divPoint) : 0)
            reizeString += "height_\(height!)"
        }
        
        if cacheKey != nil {
            key = cacheKey!
        } else {
            if width != nil || height != nil {
                key = urlString + reizeString
            } else {
                key = urlString
            }
        }
        let url = URL(string: urlString)!
        /// 查看是否已缓存小图
        if ImageCache.default.imageCachedType(forKey: key) == .none {
            self.image = nil
            /// 未缓存
            /// 查看是否有原图的缓存
            if ImageCache.default.imageCachedType(forKey: urlString) == .none {
                /// 未缓存，开始下载
                /// 显示加载控件
                let maybeIndicator = kf.indicator
                maybeIndicator?.startAnimatingView()
                /// 未缓存，从web下载图片，并缓存
                let options: KingfisherOptionsInfo = [.backgroundDecode, .fromMemoryCacheOrRefresh]
                imageDownloadTask = ImageDownloader.default.downloadImage(with: url, options: options, progressBlock: progressBlock) { [weak self] (result) in
                    maybeIndicator?.stopAnimatingView()
                    self?.imageDownloadTask = nil
                    switch result {
                        case .success(let loadingResult): // ImageLoadingResult
                            let originData = loadingResult.originalData
                            let originImage = loadingResult.image
                            var resultImage: UIImage?
                            /// 取大的倍数缩放
                            var scale: CGFloat?
                            if width != nil {
                                scale = CGFloat(width!) / originImage.size.width
                            }
                            if height != nil {
                                let heightScale = CGFloat(height!) / originImage.size.height
                                scale = scale == nil ? heightScale : max(scale!, heightScale)
                            }
                            if scale != nil {
                                resultImage = originImage.reSize(scale: scale!, originData: originData)
                            } else {
                                resultImage = originImage
                            }
                            /// 缓存图片
                            if resultImage != nil {
                                ImageCache.default.store(resultImage!, forKey: key)
                            }
                            if cacheOriginImage {
                                ImageCache.default.store(originImage, original: loadingResult.originalData, forKey: urlString)
                            }
                            completionHandler?(.success(resultImage))
                            return
                        case .failure(let error): // KingfisherError
                            completionHandler?(.failure(error))
                            return
                    }
                }
            } else {
                /// 已缓存原图，从缓存中获取原图图片展示
                // 直接获取图片（获取后才知道是 memory 还是 disk）
                ImageCache.default.retrieveImage(forKey: urlString) { (result) in
                    switch result {
                        case .success(let loadingResult): // ImageLoadingResult
                            guard let imageData = loadingResult.image else {
                                completionHandler?(.failure(.cacheError(reason: .imageNotExisting(key: urlString))))
                                return
                            }
                            var resultImage: UIImage?
                            /// 取大的倍数缩放
                            var scale: CGFloat?
                            if width != nil {
                                scale = CGFloat(width!) / imageData.size.width
                            }
                            if height != nil {
                                let heightScale = CGFloat(height!) / imageData.size.height
                                scale = scale == nil ? heightScale : max(scale!, heightScale)
                            }
                            if scale != nil {
                                resultImage = imageData.reSize(scale: scale!)
                            } else {
                                resultImage = imageData
                            }
                            /// 缓存图片
                            if resultImage != nil {
                                ImageCache.default.store(imageData, forKey: urlString)
                                ImageCache.default.store(resultImage!, forKey: key)
                            }
                            completionHandler?(.success(resultImage))
                            return
                        case .failure(let error): // KingfisherError
                            completionHandler?(.failure(error))
                            return
                    }
                }
            }
        } else {
            /// 已缓存小图，从缓存中获取图片展示
            // 直接获取图片（获取后才知道是 memory 还是 disk）
            ImageCache.default.retrieveImage(forKey: key) { (result) in
                switch result {
                    case .success(let loadingResult): // ImageLoadingResult
                        guard let imageData = loadingResult.image else {
                            completionHandler?(.failure(.cacheError(reason: .imageNotExisting(key: urlString))))
                            return
                        }
                        completionHandler?(.success(imageData))
                        return
                    case .failure(let error): // KingfisherError
                        completionHandler?(.failure(error))
                        return
                }
            }
        }
    }
    
    /// 取消下载
    func cancelLoadWebImage() {
        imageDownloadTask?.cancel()
    }
}
