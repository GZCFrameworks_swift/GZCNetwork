//
//  CollectionInlineItemType.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/9/18.
//

import Foundation

public protocol InlineCollectionItemType: BaseInlineRowType, TypedCollectionItemType {

    associatedtype InlineItem: CollectionItem, TypedCollectionItemType

    // 首次显示内联Item之前配置这个Item
    func setupInlineRow(_ inlineRow: InlineItem)
}

extension InlineCollectionItemType where Self: CollectionItem, Self.Cell.Value ==  Self.InlineItem.Cell.Value {

    /// 当前行被选中后将在其下一个位置插入(展开)的item
    public var inlineItem: Self.InlineItem? { return _inlineItem as? Self.InlineItem }

    /// 展开（打开）内联行。
    public func expandInlineRow() {
        guard inlineItem == nil else {
            return
        }
        if let section = section, let form = section.form as? CollectionForm {
            let inline = InlineItem.init(title: nil, tag: nil)
            inline.value = value
            setupInlineRow(inline)
            if (form.inlineRowHideOptions ?? CollectionForm.defaultInlineRowHideOptions).contains(.AnotherInlineRowIsShown) {
                for row in  form.allRows {
                    if let inlineRow = row as? BaseInlineRowType {
                        inlineRow.collapseInlineRow()
                    }
                }
            }
            if
                let onExpandInlineRowCallback = onExpandInlineRowCallback,
                let cell = cell
            {
                onExpandInlineRowCallback(cell, self, inline)
            }
            if let indexPath = indexPath {
                _inlineItem = inline
                section.insert(inline, at: indexPath.row + 1)
                cell?.collectionHandler()?.rowsHaveBeenAdded([inline], at: [IndexPath(row: indexPath.row + 1, section: indexPath.section)])
//                cell?.collectionHandler()?.makeRowVisible(inline)
            }
        }
    }

    /// 折叠（关闭）内联行
    public func collapseInlineRow() {
        if let selectedRowPath = indexPath, let inlineRow = _inlineItem, let inlineRowIndex = inlineRow.indexPath {
            if
                let onCollapseInlineRowCallback = onCollapseInlineRowCallback,
                let cell = cell
            {
                onCollapseInlineRowCallback(cell, self, inlineRow as! InlineItem)
            }
            
            _inlineItem = nil
            section?.remove(at: inlineRowIndex.row)
            cell?.collectionHandler()?.rowsHaveBeenRemoved([inlineRow], at: [IndexPath(row: inlineRowIndex.row, section: selectedRowPath.section)])
        }
    }

    /// 更改内联行的状态（展开/折叠）
    public func toggleInlineRow() {
        if let _ = inlineItem {
            collapseInlineRow()
        } else {
            expandInlineRow()
        }
    }

    /// 设置扩展行时要执行的Block
    @discardableResult
    public func onExpandInlineRow(_ callback: @escaping (Cell, Self, InlineItem) -> Void) -> Self {
        callbackOnExpandInlineRow = callback
        return self
    }

    /// 设置折叠行时要执行的Block
    @discardableResult
    public func onCollapseInlineRow(_ callback: @escaping (Cell, Self, InlineItem) -> Void) -> Self {
        callbackOnCollapseInlineRow = callback
        return self
    }

    /// 返回此行展开时将执行的Block
    public var onCollapseInlineRowCallback: ((Cell, Self, InlineItem) -> Void)? {
        return callbackOnCollapseInlineRow as! ((Cell, Self, InlineItem) -> Void)?
    }

    /// 返回此行折叠时将执行的Block
    public var onExpandInlineRowCallback: ((Cell, Self, InlineItem) -> Void)? {
        return callbackOnExpandInlineRow as! ((Cell, Self, InlineItem) -> Void)?
    }

    public var isExpanded: Bool { return _inlineItem != nil }
    public var isCollapsed: Bool { return !isExpanded }
}
