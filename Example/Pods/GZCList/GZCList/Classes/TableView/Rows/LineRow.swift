//
//  LineRow.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/9/11.
//

import Foundation
import UIKit
import SnapKit

// MARK:- LineCell
/// 分割线的cell，这里的value 没啥用
open class LineCell: TableCellOf<Bool> {
    
    open override func setup() {
        super.setup()
        selectionStyle = .none
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
}

// MARK:- LineRow
/// 定义好的分割线Row，可自定义线的宽度、圆角、内容边距、线的颜色以及背景色
public final class LineRow: TableRowOf<LineCell>, RowType {
    
    /// 线的颜色
    public var lineColor: UIColor = .lightGray {
        didSet {
            contentBgColor = lineColor
        }
    }
    /// 线的圆角
    public var lineRadius: CGFloat = 0 {
        didSet {
            cornerRadius = lineRadius
        }
    }
    /// 线的宽度
    public var lineWidth: CGFloat = 0.5 {
        didSet {
            cellHeight = lineWidth + contentInsets.top + contentInsets.bottom
        }
    }
    
    // 更新cell的布局
    public override func customUpdateCell() {
        super.customUpdateCell()
        guard let cell = cell else {
            return
        }
        cell.contentView.snp.updateConstraints { (make) in
            make.edges.equalTo(contentInsets)
        }
        cell.layoutIfNeeded()
    }
    
    public override var identifier: String {
        return "LineRow"
    }
    
    public required init(title: String? = nil, tag: String? = nil) {
        super.init(title: title, tag: tag)
        contentInsets = .zero
        contentBgColor = .lightGray
        cellHeight = 0.5
    }
}
