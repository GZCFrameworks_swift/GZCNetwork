//
//  GZCTableViewSection.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/9/3.
//

import Foundation

open class TableSection: Section<TableRow> {
    
    // MARK:- 初始化
    required public init() {
        super.init()
    }
    
    required public init<S>(_ elements: S) where S : Sequence, S.Element == TableRow {
        super.init(elements)
    }
    /// 初始化并在完成时回调
    public init(_ initializer: (TableSection) -> Void) {
        super.init()
        initializer(self)
    }
    public init(_ header: String?,_ initializer: (TableSection) -> Void = { _ in }) {
        super.init()
        if let header = header {
            self.header = TableHeaderFooterView(stringLiteral: header)
        }
        initializer(self)
    }
    public init(footer: String?, _ initializer: (TableSection) -> Void = { _ in }) {
        super.init()
        if let footer = footer {
            self.footer = TableHeaderFooterView(stringLiteral: footer)
        }
        initializer(self)
    }
    public init(header: String?, footer: String?, _ initializer: (TableSection) -> Void = { _ in }) {
        super.init()
        if let header = header {
            self.header = TableHeaderFooterView(stringLiteral: header)
        }
        if let footer = footer {
            self.footer = TableHeaderFooterView(stringLiteral: footer)
        }
        initializer(self)
    }
    
    // MARK:- row的隐藏与显示
    /// 隐藏Row
    func hide(row: TableRow) {
        row._cell?.cellResignFirstResponder()
        (row as? BaseInlineRowType)?.collapseInlineRow()
        guard let rowIndex = _visibleRows.firstIndex(of: row) else {
            return
        }
        _visibleRows.remove(at: rowIndex)
        guard
            let delegate = form?.delegate,
            let sectionIndex = form?.firstIndex(of: self)
        else {
            return
        }
        delegate.rowsHaveBeenRemoved([row], at: [IndexPath(row: rowIndex, section: sectionIndex)])
    }

    /// 显示Row
    func show(row: TableRow) {
        guard !_visibleRows.contains(row) else { return }
        guard var index = _allRows.firstIndex(of: row) else { return }
        var formIndex = NSNotFound
        while formIndex == NSNotFound && index > 0 {
            index = index - 1
            let previous = _allRows[index]
            formIndex = _visibleRows.firstIndex(of: previous) ?? NSNotFound
        }
        let rowIndex = formIndex == NSNotFound ? 0 : formIndex + 1
        _visibleRows.insert(row, at: rowIndex)
        guard
            let delegate = form?.delegate,
            let sectionIndex = form?.firstIndex(of: self)
        else {
            return
        }
        delegate.rowsHaveBeenAdded([row], at: [IndexPath(row: rowIndex, section: sectionIndex)])
        row._cell?.tableHandler()?.makeRowVisible(row)
    }
    
    // MARK:- header 和 footer
    /// section的header
    public var header: TableHeaderFooterViewRepresentable? {
        willSet {
            headerView = nil
        }
    }
    public var headerView: UIView?

    /// section的footer
    public var footer: TableHeaderFooterViewRepresentable? {
        willSet {
            footerView = nil
        }
    }
    public var footerView: UIView?
}

// MARK:- 可编辑的section
open class TableMultivalusedSection: TableSection {
    public var multivaluedOptions: MultivaluedOptions
    public var showInsertIconInAddButton = true
    // 创建新的row的block，触发添加时会调用
    public var multivaluedRowToInsertAt: ((Int) -> TableRow)?
    // 创建新建按钮的row的block，调用这个block来获取添加行的row
    public var addButtonProvider: ((TableMultivalusedSection) -> TableRow)!
    
    public required init(multivaluedOptions: MultivaluedOptions = MultivaluedOptions.Insert.union(.Delete),
                         header: String? = nil,
                         footer: String? = nil,
                         _ initializer: (TableMultivalusedSection) -> Void = { _ in }) {
        self.multivaluedOptions = multivaluedOptions
        super.init(header: header, footer: footer, {section in initializer(section as! TableMultivalusedSection) })
        guard multivaluedOptions.contains(.Insert) else { return }
        initialize()
    }

    public required init() {
        self.multivaluedOptions = MultivaluedOptions.Insert.union(.Delete)
        super.init()
        initialize()
    }

    public required init<S>(_ elements: S) where S : Sequence, S.Element == TableRow {
        self.multivaluedOptions = MultivaluedOptions.Insert.union(.Delete)
        super.init(elements)
        initialize()
    }

    func initialize() {
        let addRow = addButtonProvider(self)
        addRow.onCellSelection { cell, row in
            guard !row.isDisabled else { return }
            guard let tableView = cell.tableHandler()?.tableView, let indexPath = row.indexPath else { return }
            cell.tableHandler()?.tableView(tableView, commit: .insert, forRowAt: indexPath)
        }
        self <<< addRow
    }
}

// MARK:- 单选/多选列表section
/// SelectableSection中所有的row都需要遵循的协议
public protocol SelectableTableRowType: TypedTableRowType {
    var selectableValue: Cell.Value? { get set }
}

/// SelectableSection实现的协议，方便定制
public protocol SelectableTableSectionType: Collection {
    associatedtype SelectableRow: TableRow, SelectableTableRowType
    /// 单选还是多选
    var selectionType: SelectionType { get set }

    /// 选中某一行的回调
    var onSelectSelectableRow: ((SelectableRow.Cell, SelectableRow) -> Void)? { get set }

    /// 已选择的Row
    func selectedRow() -> SelectableRow?
    func selectedRows() -> [SelectableRow]
}

extension SelectableTableSectionType where Self: TableSection {
    /// 获取单选的选中Row（SingleSelection使用）
    public func selectedRow() -> SelectableRow? {
        return selectedRows().first
    }

    /// 获取多选的所有选中Row（multipleSelection使用）
    public func selectedRows() -> [SelectableRow] {
        var findRows: [SelectableRow] = []
        for row in self._visibleRows {
            if let r = row as? SelectableRow,
               r.value != nil {
                findRows.append(r)
            }
        }
        return findRows
    }

    /// Rows添加到section之前调用的函数
    func prepare(selectableRows rows: [TableRow]) {
        for row in rows {
            if let sRow = row as? SelectableRow {
                sRow.onCellSelection { [weak self] cell, row in
                    guard let s = self, !row.isDisabled else { return }
                    switch s.selectionType {
                        case .multipleSelection:
                            sRow.value = sRow.value == nil ? sRow.selectableValue : nil
                        case let .singleSelection(enableDeselection):
                            for r in s._visibleRows {
                                guard
                                    let selectableRow = r as? SelectableRow,
                                    selectableRow.value != nil,
                                    selectableRow != row
                                else { return }
                                r.baseValue = nil
                                r.updateCell()
                            }
                            // 检查是否已选中
                            if sRow.value == nil {
                                sRow.value = sRow.selectableValue
                            } else if enableDeselection {
                                sRow.value = nil
                        }
                    }
                    sRow.updateCell()
                    s.onSelectSelectableRow?(cell as! Self.SelectableRow.Cell, sRow)
                }
            }
        }
    }

}

/// A subclass of Section that serves to create a section with a list of selectable options.
open class SelectableTableSection<Row>: TableSection, SelectableTableSectionType where Row: SelectableTableRowType, Row: TableRow {

    public typealias SelectableRow = Row

    /// Defines how the selection works (single / multiple selection)
    public var selectionType = SelectionType.singleSelection(enableDeselection: true)

    /// A closure called when a row of this section is selected.
    public var onSelectSelectableRow: ((Row.Cell, Row) -> Void)?

    public override init(_ initializer: (SelectableTableSection<Row>) -> Void) {
        super.init({ _ in })
        initializer(self)
    }

    public init(_ header: String?, selectionType: SelectionType, _ initializer: (SelectableTableSection<Row>) -> Void = { _ in }) {
        self.selectionType = selectionType
        super.init(header, { _ in })
        initializer(self)
    }

    public init(header: String?, footer: String?, selectionType: SelectionType, _ initializer: (SelectableTableSection<Row>) -> Void = { _ in }) {
        self.selectionType = selectionType
        super.init(header: header, footer: footer, { _ in })
        initializer(self)
    }

    public required init() {
        super.init()
    }

    public required init<S>(_ elements: S) where S : Sequence, S.Element == TableRow {
        super.init(elements)
    }

    open override func rowsHaveBeenAdded(_ rows: [BaseRow], at: IndexSet) {
        prepare(selectableRows: rows as! [TableRow])
    }
}
