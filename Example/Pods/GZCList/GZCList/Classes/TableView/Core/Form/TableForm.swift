//
//  TableForm.swift
//  GZCList
//
//  Created by Guo ZhongCheng on 2020/9/3.
//

import Foundation

public final class TableForm: Form<TableSection> {
    /// 定义何时隐藏内联行的默认选项，仅当“inlineRowHideOptions”为nil时才适用。
    public static var defaultInlineRowHideOptions = InlineRowHideOptions.FirstResponderChanges.union(.AnotherInlineRowIsShown)

    /// 定义何时隐藏内联行的选项。如果为空，则使用“defaultInlineRowHideOptions”
    public var inlineRowHideOptions: InlineRowHideOptions?
    
}
