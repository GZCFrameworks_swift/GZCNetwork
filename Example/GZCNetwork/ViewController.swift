//
//  ViewController.swift
//  GZCNetwork
//
//  Created by Guo ZhongCheng on 10/16/2020.
//  Copyright (c) 2020 Guo ZhongCheng. All rights reserved.
//

import GZCNetwork
import GZCList

class ViewController: FormTableViewController {
    
    let section = TableSection()
    var page: Int = 0
    
    // Rx订阅垃圾桶
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        form +++ section
        
        addHeaderRefresher { [weak self] in
            self?.getData()
        }
        addFooterRefresher { [weak self] in
            self?.getMore()
        }
        
        getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData() {
        page = 0
        NetworkManager.request(DemoServerAPI.test(page)) {[weak self] (response, isCache, error) in
            if let error = error {
                print("请求失败：\(error.localizedDescription)")
            }
            guard let response = response else { return }
            print("json -- \(response.fetchJSONString())")
            let list = response.mapArray(DemoModel.self)
            self?.updateImages(list)
            self?.stopRefreshing()
        }
    }
    
    func getMore() {
        page += 1
//        NetworkManager.request(DemoServerAPI.test(page)) {[weak self] (response, isCache, error) in
//            guard let response = response else { return }
//
//            let list = response.mapArray(DemoModel.self)
//
//            self?.addImages(list)
//            if self?.page ?? 0 > 10 {
//                self?.noticeNoMoreData()
//            } else {
//                self?.stopLoadingMore()
//            }
//        }
        RxNetworkManager.request(DemoServerAPI.test(page))
        .mapArray(DemoModel.self)
        .subscribe(onNext: {[weak self] (list: [DemoModel]) in
            // 转换对象成功
            self?.addImages(list)
        }, onError: { (error) in
            // 请求失败
            print(error.localizedDescription)
        }, onCompleted: {
            // onNext之后执行
            print("onCompleted")
        }, onDisposed: { [weak self] in
            // 订阅取消，成功和失败最后都会走到 onDisposed 这里，因此在这里结束动画
            if self?.page ?? 0 > 10 {
                self?.noticeNoMoreData()
            } else {
                self?.stopLoadingMore()
            }
        }).disposed(by: disposeBag)
    }
    
    func updateImages(_ images: [DemoModel]) {
        var imageRows = [ImageRow]()
        for image in images {
            imageRows += ImageRow(url: image.coverUrl ?? "")
        }
        section >>> imageRows
//        form >>> [section]
    }

    func addImages(_ images: [DemoModel]) {
//        let newSection = TableSection()
        var imageRows = [ImageRow]()
        for image in images {
            imageRows += ImageRow(url: image.coverUrl ?? "")
        }
        section <<<! imageRows
//        form +++! newSection
    }
}

