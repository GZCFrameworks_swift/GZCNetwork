//
//  AppDelegate.swift
//  GZCNetwork
//
//  Created by Guo ZhongCheng on 10/16/2020.
//  Copyright (c) 2020 Guo ZhongCheng. All rights reserved.
//

import UIKit
import GZCNetwork

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // 设置请求返回json格式
        NetworkConfig.shared.customPlugins = [MoyaMapperPlugin(GZCNetworkResponse(), transformError: false)]
        
        // 设置请求头加密
        NetworkConfig.shared.onHeaderEncryption { (method, header, params, body) -> [String : String] in
            return header ?? [:]
        }
        // 设置参数回调
        NetworkConfig.shared.onResultHandle { (url, result, isCache, error) in
            // 返回true表示继续执行后续操作
            return true
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

