//
//  DemoModel.swift
//  GZCNetwork_Example
//
//  Created by Guo ZhongCheng on 2020/10/21.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import MoyaMapper
import SwiftyJSON

//MARK: - 居物志
struct DemoModel: Equatable, Modelable{
    
    // 封面图片
    var coverUrl : String?
    // 观看人数
    var reviewCount: Int?
    
    mutating func mapping(_ json: JSON) {
        self.reviewCount = json["reviewCount"].intValue
    }
}
