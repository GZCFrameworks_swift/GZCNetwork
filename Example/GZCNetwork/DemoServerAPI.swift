//
//  DemoServerAPI.swift
//  GZCNetwork_Example
//
//  Created by Guo ZhongCheng on 2020/10/21.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCNetwork

// 请求枚举，请求的地方使用它发起请求
enum DemoServerAPI {
    case test(_ page: Int = 0)
}

// 扩展实现GZCTargetType协议，定义枚举值的具体请求内容
extension DemoServerAPI: GZCTargetType {
    // base路径，也可根据self枚举返回不一样的base路径
    var baseURL: URL {
        return URL(string: "http://mockhttp.cn/mock")!
    }
    
    // 后缀路径，根据self枚举返回不一样的具体后缀路径
    var path: String {
        switch self {
            case .test:
                return "GZCNetTest1"
        }
    }
    
    // 请求方法，可根据self枚举返回不一样的请求方法
    var method: HTTPMethod {
        return .get
    }
    
    // 请求超时时间，不重写时将会使用NetworkConfig.timeoutInterval对应值（可配置）
    var timeoutInterval: TimeInterval {
        return 10
    }
    
    // 是否需要缓存，如果返回值为true，且已有缓存，则会先返回一次缓存内容，再返回一次请求结果
    var cache: Bool {
        switch self {
            case .test(let page):
                return page == 0
        }
    }
    
    // 请求失败重试设置
    var retry: RetrySetting? {
        return RetrySetting(delay: 2, maxTime: 2)
    }
}
