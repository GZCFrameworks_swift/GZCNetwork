# GZCNetwork

## 网络请求模块

网络请求支持模块，基于[`MoyaMapper`](https://github.com/MoyaMapper/MoyaMapper)（`Alamofier`>`Moya`+`SwiftyJSON`>`MoyaMapper`）的封装，支持RxSwift

## 如何安装

GZCNetwork支持[CocoaPods](https://cocoapods.org)安装。
在 Podfile 中添加下面代码:

```
pod 'GZCNetwork'
```

如果要支持RxSwift，则选择添加下面代码：

```
pod 'GZCNetwork/Rx'
```

## 使用方法

### 一、插件配置
1、定义适用于项目接口的`ModelableParameterType`：

如果服务器的返回json格式如下：

```
{
	"status": 1,
	"data": // 具体数据内容...,
	"message": "操作成功"
}
```

可使用框架中已定义好的`GZCNetworkResponse`，否则需自定义`ModelableParameterType`：

```
// statusCodeKey、tipStrKey、 modelKey 可以任意指定级别的路径，如： "error>used"
struct NetResponseType: ModelableParameterType {
    var successValue = "1"        // 请求成功时后台返回的状态码（statusCodeKey对应值），如:"1"
    var statusCodeKey = "status"  // 请求成功时后台返回的状态Key, 如:"status"
    var tipStrKey = "message"     // 后台返回请求提示信息的Key，如:"message"
    var modelKey = "data"         // 返回的数据内容所在的Key，如:"data"
}
```
2、添加插件到请求全局设置

```
NetworkConfig.customPlugins = [MoyaMapperPlugin(NetResponseType(), transformError: false)]
```

> MoyaMapperPlugin初始化的时候，transformError 可不传
>
> transformError 默认为 true，表示当请求失败的时候，会自动创建一个 response 并返回出去，可通过`response.statusCode == MMStatusCode.loadFail.rawValue`判断是否请求失败
>
> 如果设置 transformError 为 false，可通过判断回调函数中的 error 是否存在来判断是否请求失败

### 二、创建Model

创建Model类型/结构体, 并实现协议`Modelable`即可，嵌套Model时，子Model也需要实现该协议 ：
```
struct CompanyModel: Modelable {
    
    var name : String = ""
    var catchPhrase : String = ""
    
    mutating func mapping(_ json: JSON) {
    		/// mapping中可以映射与参数名或类型不一致的值
        self.name = json["nickname"].stringValue
    }
}
```
如果请求结果已有数据的json模板，可以使用[这个网站](http://www.jsoncafe.com/)来快速创建Model：
> 1、在`Class Prefix`中输入类的前缀（如"User"），在`Root Class`中输入后缀（如"Model"）
> 2、在`Code Template`下拉框中随便选择一个，
> 3、替换`Edit Template`选项卡中的内容为我们写好的模板之一（下面模板分别对应创建class和struct）：
>
> ```
> import MoyaMapper
> import SwiftyJSON
> 
> //MARK: - {{className}}
> class {{className}} : NSObject, Modelable{
> 
>     {{#properties}}
>     var {{nativeName}} : {{type}}?
>     {{/properties}}
> 
>     func mapping(_ json: JSON) {
>     }
>     
>     required override init() {
>     }
> }
> ```
>
> ```
> import MoyaMapper
> import SwiftyJSON
> 
> //MARK: - {{className}}
> struct {{className}}:Equatable, Modelable {
> 
>     {{#properties}}
>     var {{nativeName}} : {{type}}?
>     {{/properties}}
>     
>     mutating func mapping(_ json: JSON) {
>     }
> }
> ```
>
> 4、将`Json`选项卡中的内容替换成json数据，然后点击`Generate`按钮，即可在右侧看到生成好的内容。
>
> 5、检查各属性的类型并修正（如果json数据中的数字是整数，这里会自动转成Int类型，需要注意是否实际数据也需要的是整数）

### 三、创建API文件

每个功能模块都可以根据实际情况创建自己的API管理文件，如：

```
import GZCNetwork

// 请求枚举，请求的地方使用它发起请求
enum DemoServerAPI {
    case test(_ page: Int = 0)
}

// 扩展实现GZCTargetType协议，定义枚举值的具体请求内容
extension DemoServerAPI: GZCTargetType {
    // base路径，也可根据self枚举返回不一样的base路径
    var baseURL: URL {
        return URL(string: "http://mockhttp.cn/mock")!
    }
    
    // 后缀路径，根据self枚举返回不一样的具体后缀路径
    var path: String {
        switch self {
            case .test:
                return "GZCNetTest1"
        }
    }
    
    // 请求方法，可根据self枚举返回不一样的请求方法
    var method: HTTPMethod {
        return .get
    }
    
    // 请求超时时间，不重写时将会使用NetworkConfig.timeoutInterval对应值（可配置）
    var timeoutInterval: TimeInterval {
        return 10
    }
    
    // 是否需要缓存，如果返回值为true，且已有缓存，则会先返回一次缓存内容，再返回一次请求结果
    var cache: Bool {
        switch self {
            case .test(let page):
                return page == 0
        }
    }
}
```

### 四、发起请求

框架中提供了两种请求发起的方法：

1、回调方式发起请求：

```swift
NetworkManager.request(DemoServerAPI.test(page)) {[weak self] (response, isCache, error) in
    if let error = error {
        print("请求失败：\(error.localizedDescription)")
    }
    guard let response = response else { return }
    print("json -- \(response.fetchJSONString())")
    // 转换模型
    let list = response.mapArray(DemoModel.self)
    // 后续操作
}
```

2、使用RxSwift，使用下面方式发起请求：

```swift
RxNetworkManager.request(DemoServerAPI.test(page))
        .mapArray(DemoModel.self)
        .subscribe(onNext: {[weak self] (list: [DemoModel]) in
            // 转换对象成功
            self?.addImages(list)
        }, onError: { (error) in
            // 请求失败
            print(error.localizedDescription)
        }, onCompleted: {
            // onNext之后执行
            print("onCompleted")
        }, onDisposed: { [weak self] in
            // 订阅取消，成功和失败最后都会走到 onDisposed 这里，因此可以在这里结束刷新动画
            print("onDisposed")
        }).disposed(by: disposeBag)
```

### 五、response结果转换

如果请求结果需要的是Model类型，则使用上面的方法可以很容易的完成转换，其中.mapArray表示转换成对应的model数组，可以替换成其他解析方法，具体如下：

| 方法            | 描述                                                         |
| --------------- | ------------------------------------------------------------ |
| toJSON          | Response 转 JSON                                             |
| fetchString     | 获取指定路径的字符串                                         |
| fetchJSONString | 获取指定路径的原始json字符串                                 |
| mapResult       | Response -> MoyaMapperResult `(Bool, String)` ，其中Bool表示是否请求成功（即 `ModelableParameterType ` 中的 `statusCodeKey` 对应值是否等于 `successValue`），String表示 `tipStrKey` 对应值 |
| mapObject       | Response 转单个 Model                                        |
| mapObjResult    | Response -> (MoyaMapperResult, Model) ，就是mapObject加上mapResult |
| mapArray        | Response -> [Model]                                          |
| mapArrayResult  | Response -> (MoyaMapperResult, [Model])，就是mapArray加上mapResult |

> 除了 `fetchJSONString` 的默认解析路径是`根路径`之外，其它方法的默认解析路径为插件对象中的 `modelKey`

如果请求结果是基本类型，可使用`.toJSON`方法的JSON对象的`.xxxValue`属性获取到对应类型的值。

### 六、请求统一配置

框架提供了统一配置类`NetworkConfig`可进行请求统一配置，可配置参数如下：

| 参数/方法            | 描述                                                         |
| -------------------- | ------------------------------------------------------------ |
| customPlugins        | 自定义插件数组，上面的NetResponseType就是通过此属性设置      |
| timeoutInterval      | 请求超时时间（Api中也可对单个接口单独设置）                  |
| shouldEncryMethods   | 与 headerEncryptionFunc 配合使用，此属性指定那些请求方法需要对Header进行 加密/签名 |
| onHeaderEncryption() | 与shouldEncryMethods 配合使用，表示对需要 加密/签名 的Header进行 加密/签名 的过程 |
| onResultHandle()     | 设置请求返回值统一预处理（用于统一处理用户登录过期等错误）   |

### 七、请求单独配置

框架同时还提供独立的请求配置方法：

1、自定义对象实现`NetworkConfigType`协议, 如:

```swift
struct CustomConfig: NetworkConfigType {
    var customPlugins: [PluginType]?
    
    var shouldEncryMethods: [HTTPMethod]
    
    var timeoutInterval: TimeInterval
    
    func headerEncryption(_ method: String, _ header: [String : String]?, _ parames: [String : Any]?, _ body: [String : Any]?) -> [String : String] {
        // 请求头签名过程
        return ...
    }
    
    func resultHandle(_ url: String, _ response: Response?, _ isCache: Bool, _ error: MoyaError?) -> Bool {
        // 请求结果预处理过程
        return true
    }
}
```



2、发起请求时，传入对应的config对象即可，如:

```swift
NetworkManager.request(DemoServerAPI.test(page), config: CustomConfig())
```

### 八、请求失败自动重试

框架中提供了请求失败重试的机制，发起请求的代码无需做任何修改，在`YHTargetType`协议实现中添加`retry`属性来设置请求重试次数和间隔时间（delay表示重试间隔时间，maxTime表示最多重试次数）即可：

```swift
// 请求失败重试设置
var retry: RetrySetting? {
    return RetrySetting(delay: 2, maxTime: 2)
}
```

> **注意**：如果使用RxSwift的自动重试时出现验签失败的情况，可能是由于重试时没有重新创建新的请求导致，需要手动在失败时重新弃用当前流并重新创建新的请求流（如果使用回调方式则每次重试都会重新创建请求）。

## Author

Guo ZhongCheng, gzhongcheng@qq.com

## License

GZCNetwork is available under the MIT license. See the LICENSE file for more info.
