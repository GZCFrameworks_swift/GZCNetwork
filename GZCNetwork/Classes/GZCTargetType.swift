//
//  GZCTargetType.swift
//  GZCNetwork
//
//  Created by Guo ZhongCheng on 2020/10/20.
//
//  拓展TargetType

import Moya

public struct RetrySetting {
    // 重连间隔（秒）
    var delay: TimeInterval
    // 最大重连次数
    var maxTime: Int
    
    public init (delay: TimeInterval = 3, maxTime: Int = 3) {
        self.delay = delay
        self.maxTime = maxTime
    }
}

public protocol GZCTargetType: TargetType {
    // 参数
    var parameters: [String: Any]? { get }
    // body
    var body: [String: Any]? { get }
    // 参数编码方式
    var parameterEncoding: ParameterEncoding { get }
    
    // 是否需要缓存
    var cache: Bool { get }
    // 缓存Key
    var cacheKey: String? { get }
    // 是否打印请求结果
    var printLog: Bool { get }
    
    // 超时时间
    var timeoutInterval: TimeInterval { get }
    
    // 重连机制, 为空时表示不重连
    var retry: RetrySetting? { get }
}

// 添加默认实现
public extension GZCTargetType {
    // 是否缓存
    var cache: Bool {
        return false
    }
    
    // 缓存key
    var cacheKey: String? {
        return (parameters?.description ?? "") + (body?.description ?? "")
    }
    
    // 是否打印请求结果
    var printLog: Bool {
        return false
    }
    
    // 参数编码方式
    var parameterEncoding: ParameterEncoding {
        if method == .get || method == .delete {
            return URLEncoding.default
        }
        return JSONEncoding.default
    }
    
    var task: Task {
        if body == nil {
            return .requestParameters(parameters: parameters ?? [:], encoding: parameterEncoding)
        }
        return .requestCompositeParameters(bodyParameters: body ?? [:], bodyEncoding: parameterEncoding, urlParameters: parameters ?? [:])
    }
    
    // 用于单元测试的测试用数据
    var sampleData: Data {
        return "GZC".data(using: .utf8)!
    }
    
    // 超时时间
    var timeoutInterval: TimeInterval {
        return NetworkConfig.shared.timeoutInterval
    }
    
    // 请求头
    var headers: [String : String]? {
        return nil
    }
    
    // 参数
    var parameters: [String : Any]? {
        return nil
    }
    
    // body
    var body: [String : Any]? {
        return nil
    }
    
    // 重连机制
    var retry: RetrySetting? {
        return nil
    }
}
