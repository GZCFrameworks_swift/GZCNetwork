//
//  RxNetworkManager.swift
//  GZCNetwork
//
//  Created by Guo ZhongCheng on 2020/10/22.
//

@_exported import RxSwift
@_exported import Moya
@_exported import MoyaMapper

public struct RxNetworkManager {
    
    static var keepProvider: Any?
    
    // MARK: Public Func
    
    /// 发起请求
    /// - Parameters:
    ///   - api: 请求Api
    ///   - responseCancledTip: 由于的resultHandleFunc返回false而取消继续处理时的错误提示信息
    /// - Returns: 请求的Observable
    @discardableResult
    static public func request<T: GZCTargetType>(_ api: T, responseCancledTip: String = "请求已取消", config: NetworkConfigType = NetworkConfig.shared) -> Observable<Response> {
        let provider = NetworkManager.shared.createProvider(api: api, config: config)
        keepProvider = provider
        let key: String = api.cacheKey ?? (api.parameters?.description ?? "") + (api.body?.description ?? "")
        var request: Observable<Response>!
        if api.cache {
            request = provider.rx.cacheRequest(api, alwaysFetchCache: true, callbackQueue: DispatchQueue.main, cacheType: .custom(key))
        } else {
            request = provider.rx.request(api, callbackQueue: DispatchQueue.main).asObservable()
        }
        return request.flatMap { (response) -> Observable<Response> in
            let isCache = response.statusCode == MMStatusCode.cache.rawValue
            if !config.resultHandle(api.path,response, isCache, nil) {
                return Observable.error(RxResponseError.responseCancled(responseCancledTip))
            }
            return Observable.just(response)
        }.retryWhen({ (error) -> Observable<Int> in
            /// 重试
            if let retry = api.retry {
                return error.enumerated().flatMap { (index, error) -> Observable<Int> in
                    guard index < retry.maxTime else {
                        return Observable.error(error)
                    }
                    return Observable<Int>.timer(retry.delay, scheduler: MainScheduler.instance)
                }
            }
            return Observable.error(error as! Error)
        })
    }
}

// MARK:- 定义请求取消错误
public enum RxResponseError: Swift.Error {
    /// 请求结果已经被处理而取消
    case responseCancled(String)
}
extension RxResponseError: LocalizedError {
    public var errorDescription: String? {
        switch self {
            case .responseCancled(let string):
                return string
        }
    }
}
extension RxResponseError: CustomNSError {
    public var errorUserInfo: [String: Any] {
        var userInfo: [String: Any] = [:]
        userInfo[NSLocalizedDescriptionKey] = errorDescription
        return userInfo
    }
}
