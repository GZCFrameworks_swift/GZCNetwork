//
//  NetworkConfig.swift
//  GZCNetwork
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import Foundation
import Moya

// MARK:- 配置协议
public protocol NetworkConfigType {
    /// 自定义的PluginType数组
    var customPlugins: [PluginType]? { get set }
    
    /// 需要添加签名header的请求方式
    var shouldEncryMethods: [HTTPMethod] { get set }
    
    /// 默认请求超时时间（秒）
    var timeoutInterval: TimeInterval { get set }
    
    /// 请求头签名方法（对于请求Method在shouldEncryMethods数组中的请求，发起请求前会调用这个代码块获取header字典）
    func headerEncryption(_ method: String,_ header: [String: String]?,_ parames: [String: Any]?,_ body:[String: Any]?) -> [String: String]?
    
    /// 请求返回值统一预处理（用于统一处理用户登录过期等错误）
    func resultHandle(_ url: String,_ response: Moya.Response?, _ isCache: Bool, _ error: MoyaError?) -> Bool
}

// MARK:- 网络请求管理
public class NetworkConfig: NetworkConfigType {
    // MARK:- block定义
    /// 参数：
    ///  method - 请求方法
    ///  header - 已有header
    ///  parames - 请求参数
    ///  body - 请求body数据
    /// 返回值： 最终的header字典
    public typealias RequestHeaderEncryptionBlock = (_ method: String,_ header: [String: String]?,_ parames: [String: Any]?,_ body:[String: Any]?) -> [String: String]?

    /// 参数：
    ///  url - 请求地址
    ///  response - 请求结果
    ///  isCache - 是否缓存数据
    ///  error - 错误信息
    /// 返回值： true表示可以继续处理， false表示不要继续处理
    public typealias ResultHandleBlock = (_ url: String,_ response: Moya.Response?, _ isCache: Bool, _ error: MoyaError?) -> Bool
    
    // MARK:- 属性
    /// 单例
    public static let shared = NetworkConfig()
    
    /// 自定义的PluginType数组
    public var customPlugins: [PluginType]?
    /// 需要添加签名header的请求方式
    public var shouldEncryMethods: [HTTPMethod] = [.get, .post, .put, .delete]
    
    /// 请求头签名方法（请求方法在shouldEncryMethods中的请求，发起请求前会调用这个代码块获取header字典）
    var headerEncryptionFunc: RequestHeaderEncryptionBlock?
    
    /// 请求返回值统一预处理（用于统一处理用户登录过期等错误）
    var resultHandleFunc: ResultHandleBlock?
    
    /// 网络请求队列的名称（必须在首次请求之前设置才有效）
    public static var queueName = "com.gzc.NetworkManager"
    
    /// 默认请求超时时间（秒）
    public var timeoutInterval: TimeInterval = 15
    
    // MARK:- 设置方法
    /// 设置请求头签名方法
    public func onHeaderEncryption(block: @escaping RequestHeaderEncryptionBlock) {
        headerEncryptionFunc = block
    }
    /// 设置请求返回值统一预处理（用于统一处理用户登录过期等错误）
    public func onResultHandle(block: @escaping ResultHandleBlock) {
        resultHandleFunc = block
    }
    
    /// 调用请求头签名
    public func headerEncryption(_ method: String, _ header: [String : String]?, _ parames: [String : Any]?, _ body: [String : Any]?) -> [String : String]? {
        return headerEncryptionFunc?(method, header, parames, body)
    }
    /// 调用请求结果预处理
    public func resultHandle(_ url: String, _ response: Response?, _ isCache: Bool, _ error: MoyaError?) -> Bool {
        return resultHandleFunc?(url, response, isCache, error) ?? true
    }
}
