//
//  GZCReachability.swift
//  GZCNetwork
//
//  Created by Guo ZhongCheng on 2020/10/20.
//
//  网络状态检测

import Reachability

public struct GZCReachability {
    /// 网络监听器
    public static var reachability = try! Reachability()
    
    /// 获取当前网络状态
    static public func currentConnectionStatus() -> Reachability.Connection {
        do {
            reachability = try Reachability()
            return reachability.connection
        } catch {
            return .unavailable
        }
    }
    
    /// 开始监听网络
    static public func startReachability() {
        do {
            try reachability.startNotifier()
        } catch {
            print("开启网络监听失败!")
        }
    }
    
    /// 停止监听网络
    static public func stopReachability() {
        reachability.stopNotifier()
    }
    
    /// 添加网络变化通知的接收者
    static public func addReachabilityObserver(_ observer: Any, selector aSelector: Selector) {
        NotificationCenter.default.addObserver(observer, selector: aSelector, name: .reachabilityChanged, object: reachability)
    }
    
    /// 网络改变的回调闭包，例如
    ///   { reachability in
    ///        if reachability.connection == .wifi {
    ///            print("当前为WiFi网络")
    ///        } else {
    ///            print("Reachable via Cellular")
    ///        }
    ///    }
    static public func setReachableCallBack(whenReachable: Reachability.NetworkReachable?) {
        reachability.whenReachable = whenReachable
    }
    
    /// 连接失败的回调闭包，例如
    ///    { _ in
    ///        print("网络连接失败")
    ///    }
    static public func setUnReachableCallBack(whenUnreachable: Reachability.NetworkUnreachable?) {
        reachability.whenUnreachable = whenUnreachable
    }
}

