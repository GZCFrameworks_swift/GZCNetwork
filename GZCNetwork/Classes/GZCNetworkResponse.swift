//
//  GZCNetworkResponse.swift
//  GZCNetwork
//
//  Created by Guo ZhongCheng on 2020/10/20.
//
//  使用MoyaMapper对服务端数据进行统一解析

import MoyaMapper

public struct GZCNetworkResponse: ModelableParameterType {
    // statusCodeKey、tipStrKey、 modelKey 可以任意指定级别的路径，如： "error>used"
    /// 请求成功时状态码对应的值
    public var successValue = "1"
    /// 状态码对应的键
    public var statusCodeKey = "status"
    /// 请求后的提示语对应的键
    public var tipStrKey = "message"
    /// 请求后的主要模型数据的键
    public var modelKey = "data"
    
    public init() {}
}
