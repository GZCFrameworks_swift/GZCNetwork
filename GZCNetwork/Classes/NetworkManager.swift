//
//  NetworkManager.swift
//  GZCNetwork
//
//  Created by Guo ZhongCheng on 2020/10/20.
//
//  网络请求管理

@_exported import Moya
@_exported import MoyaMapper

public typealias HTTPMethod = Moya.Method
public typealias GZCResult = Result<Moya.Response, MoyaError>
public typealias GZCProgress = (Double) -> ()
public typealias GZCCompletion = (_ response: Moya.Response?, _ isCache: Bool, _ error: MoyaError?) -> Void

public struct NetworkManager {
    
    // MARK: Public Func
    // 发起请求
    @discardableResult
    static public func request<T: GZCTargetType>(
        _ api: T,
        config: NetworkConfigType = NetworkConfig.shared,
        progressBlock: GZCProgress? = nil,
        completion: @escaping GZCCompletion) -> Cancellable? {
        
        var shared = NetworkManager.shared
        // 判断是否相同的请求
        if shared.isSameRequest(api) {
            return nil
        }
        
        let provider = shared.createProvider(api: api, config: config)
        var request: Cancellable!
        if api.cache {
            let key: String = api.cacheKey ?? (api.parameters?.description ?? "") + (api.body?.description ?? "")
            request = provider.cacheRequest(api, alwaysFetchCache: true, cacheType: .custom(key), callbackQueue: DispatchQueue.main, progress: { (progress) in
                progressBlock?(progress.progress)
            }) { (response) in
                // 处理响应
                shared.handleResponse(api: api, config: config, result: response, progress: progressBlock, completion: completion)
                // 请求完成移除
                shared.cleanRequest(api)
            }
        } else {
            request = provider.request(api, callbackQueue: DispatchQueue.main, progress: { (progress) in
                progressBlock?(progress.progress)
            }) { (response) in
                // 处理响应
                shared.handleResponse(api: api, config: config, result: response, progress: progressBlock, completion: completion)
                // 请求完成移除
                shared.cleanRequest(api)
            }
        }
        
        shared.addRequest(url: api.baseURL.absoluteString + api.path, request: request)
        
        return request
    }
    
    // MARK: Private Func
    // 创建Moya请求体
    func createProvider<T: GZCTargetType>(api: T, config: NetworkConfigType) -> MoyaProvider<T> {
        // hud插件
        let activityPlugin = NetworkActivityPlugin { (state, targetType) in
            switch state {
                case .began:
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    }
                case .ended:
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        }
        
        
        var plugins: [PluginType] = [activityPlugin]
        
        if let customPlugins = config.customPlugins {
            plugins.append(contentsOf: customPlugins)
        }
        
        let provider = MoyaProvider<T>(endpointClosure: { (api) -> Endpoint in
            return self.encryptHeaderEndpoint(api: api, config: config)
        }, requestClosure: { (endPoint, closure) in
            do {
                var urlRequest = try endPoint.urlRequest()
                urlRequest.timeoutInterval = api.timeoutInterval;
                closure(.success(urlRequest))
            } catch MoyaError.requestMapping(let url) {
                closure(.failure(MoyaError.requestMapping(url)))
            } catch MoyaError.parameterEncoding(let error) {
                closure(.failure(MoyaError.parameterEncoding(error)))
            } catch {
                closure(.failure(MoyaError.underlying(error, nil)))
            }
        }, plugins: plugins)
        
        return provider
    }
    
    // 通过endpoint添加加密header
    private func encryptHeaderEndpoint<T: GZCTargetType>(api: T, config: NetworkConfigType) -> Endpoint {
        let endPoint = MoyaProvider.defaultEndpointMapping(for: api)
        if config.shouldEncryMethods.contains(api.method) {
            var encryptionHeader: [String: String]?
            switch api.task {
            case let .requestParameters(parameters, _):
                encryptionHeader = config.headerEncryption(api.method.rawValue.lowercased(), nil, parameters, nil)
            case let .requestCompositeParameters(bodyParameters, _, urlParameters):
                encryptionHeader = config.headerEncryption(api.method.rawValue.lowercased(), nil, urlParameters, bodyParameters)
            default:
                break
            }
            
            if let header = encryptionHeader {
                return endPoint.adding(newHTTPHeaderFields: header)
            }
        }
        
        return endPoint
    }
    
    
    // MARK: Properties
    // 单例
    public static var shared = NetworkManager()
    
    /// private
    // 栅栏队列
    private lazy var barrierQueue = DispatchQueue(label: NetworkConfig.queueName, attributes: .concurrent)
    // 重连队列
    private lazy var retryQueue = DispatchQueue(label: NetworkConfig.queueName + ".retry", attributes: .concurrent)
    // 当前所有请求
    private var requestCache = [String: [Cancellable]?]()
    // 用于控制重复请求
    private var fetchRequestKeys = [String]()
    
    // 记录重试Api次数
    private lazy var apiReturyTime = [String:Int]()
}


// MARK: - 处理响应数据
extension NetworkManager {
    
    func handleResponse<R: GZCTargetType>(api: R, config: NetworkConfigType, result: GZCResult, progress: GZCProgress?, completion: @escaping GZCCompletion) {
        switch result {
            case .success(let response):
                let isCache = response.statusCode == MMStatusCode.cache.rawValue
                if config.resultHandle(api.path, response, isCache, nil) {
                    DispatchQueue.main.async {
                        completion(response, isCache, nil)
                    }
                }
            case .failure(let error):
                var key: String = "NONE"
                switch api.task {
                    case let .requestParameters(parameters, _):
                        key = api.path + parameters.description
                    case let .requestCompositeParameters(body, _,params):
                        key = api.path + body.description + params.description
                    default:
                        // 不会调用
                        break
                }
                // 重连机制
                let currentTime: Int = NetworkManager.shared.apiReturyTime[key] ?? 0
                if let retry = api.retry, currentTime < retry.maxTime {
                    NetworkManager.shared.apiReturyTime[key] = currentTime + 1
                    NetworkManager.shared.retryQueue.asyncAfter(deadline: DispatchTime.now() + retry.delay) {
                        NetworkManager.request(api, config: config, progressBlock: progress, completion: completion)
                    }
                } else {
                    // 移除
                    NetworkManager.shared.apiReturyTime[key] = nil
                    DispatchQueue.main.async {
                        completion(nil, false, error)
                    }
                }
        }
    }
    
}


// MARK: - 请求控制
extension NetworkManager {
    
    // MARK: Public Func
    // 取消指定url的所有网络请求
    static public func cancel(url: String) {
        NetworkManager.shared.barrierQueue.sync(flags: .barrier) {
            var requestCache = NetworkManager.shared.requestCache
            if requestCache[url] != nil {
                let requests:[Cancellable] = requestCache[url]!!
                for request in requests {
                    if !request.isCancelled {
                        request.cancel()
                    }
                }
                requestCache[url] = nil
            }
        }
    }
    
    // 取消全部网络请求
    static public func cancelAll() {
        NetworkManager.shared.barrierQueue.sync(flags: .barrier) {
            var requestCache = NetworkManager.shared.requestCache
            for value in requestCache.values {
                let requests:[Cancellable] = value!
                for request in requests {
                    if request.isCancelled == false {
                        request.cancel()
                    }
                }
            }
            requestCache.removeAll()
        }
    }
    
    // MARK: Private Func
    // 添加网络请求
    mutating func addRequest(url: String, request: Cancellable) {
        if requestCache[url] != nil {
            var requests:[Cancellable] = requestCache[url]!!
            requests.append(request)
        } else {
            var requests:[Cancellable] = []
            requests.append(request)
            requestCache[url] = requests
        }
    }
    
    // 保证同一请求同一时间只请求一次
    mutating func isSameRequest<T: GZCTargetType>(_ api: T) -> Bool {
        switch api.task {
        case let .requestParameters(parameters, _):
            let key = api.path + parameters.description
            var result = false
            barrierQueue.sync(flags: .barrier) {
                result = fetchRequestKeys.contains(key)
                if !result {
                    fetchRequestKeys.append(key)
                }
            }
            return result
        case let .requestCompositeParameters(body, _,params):
            let key = api.path + body.description + params.description
            var result = false
            barrierQueue.sync(flags: .barrier) {
                result = fetchRequestKeys.contains(key)
                if !result {
                    fetchRequestKeys.append(key)
                }
            }
            return result
        default:
            // 不会调用
            return false
        }
    }
    
    mutating func cleanRequest<T: GZCTargetType>(_ api: T) {
        switch api.task {
        case let .requestParameters(parameters, _):
            let key = api.path + parameters.description
            barrierQueue.sync(flags: .barrier) {
                if let index = fetchRequestKeys.firstIndex(of: key) {
                    fetchRequestKeys.remove(at: index)
                }
            }
        case let .requestCompositeParameters(body, _,params):
            let key = api.path + body.description + params.description
            barrierQueue.sync(flags: .barrier) {
                if let index = fetchRequestKeys.firstIndex(of: key) {
                    fetchRequestKeys.remove(at: index)
                }
            }
        default:
            // 不会调用
            break
        }
    }
    
}

